# Gpypaint


## Building GpyPaint

Run the follow commands to build and run:

```sh
flatpak-builder --user app org.janwillem.gpypaint.json --force-clean
flatpak-builder --run app org.janwillem.gpypaint.json gpypaint

```

## Tools

- Selection Tools
    - Rectangle Select
    - Lasso Select
    - Ellipse Select
    - Magic Wand
- Move Tools
    - Move Selected Pixels
    - Move Selection
- Fill Tools
    - Paint Bucket
    - Gradient
- Drawing Tools
    - Paintbrush
    - Eraser
    - Pencil
- Photo Tools
    - Color Picker
    - Clone Stamp
    - Recolor Tool
- Text and Shape Tools
    - Text Tool
    - Line/Curve Tool
    - Shapes Tool
