# window.py
#
# Copyright 2022 Jan Willem Eriks
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gtk, Adw, cairo
from .canvas import Canvas


@Gtk.Template(resource_path='/org/janwillem/gpypaint/ui/window.ui')
class gpypaintWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'gpypaintWindow'

    drawingArea:Canvas = Gtk.Template.Child("Canvas")
    zoom_scale = Gtk.Template.Child("zoom-scale")
    tools_bar = Gtk.Template.Child("tools-bar")

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_title("GpyPaint")

        toolbar:Gtk.Builder = Gtk.Builder.new_from_resource("/org/janwillem/gpypaint/ui/tools-bar.ui")
        self.tools_bar.append(toolbar.get_object("leaf-toolbar"))

        self.drawingArea.create_render_buffer(500, 500)
        self.zoom_scale.set_range(0.1, 20)
        self.zoom_scale.set_draw_value(True)
        self.zoom_scale.set_value(20)
        self.zoom_scale.connect("value-changed", lambda x: self.drawingArea.set_zoom(x.get_value()))


class AboutDialog(Gtk.AboutDialog):

    def __init__(self, parent):
        Gtk.AboutDialog.__init__(self)
        self.props.program_name = 'gpypaint'
        self.props.version = "0.1.0"
        self.props.authors = ['Jan Willem Eriks']
        self.props.copyright = '2022 Jan Willem Eriks'
        self.props.logo_icon_name = 'org.janwillem.gpypaint'
        self.props.modal = True
        self.set_transient_for(parent)