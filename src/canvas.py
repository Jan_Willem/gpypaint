import cairo

from gi.repository import Gtk, GdkPixbuf, Gdk

class Canvas(Gtk.DrawingArea):
    __gtype_name__ = 'CanvasWidget'

    zoom = 1
    org_canvas_size = (-1,-1)
    canvas_pos = (-1, -1)


    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_draw_func(self.draw)

        drag_listener = Gtk.GestureDrag()
        drag_listener.connect("drag-update", self.on_drag)
        self.add_controller(drag_listener)

        zoom_listener = Gtk.GestureZoom()
        self.add_controller(zoom_listener)
        zoom_listener.connect("scale-changed", self.on_zoom)
        zoom_listener.connect("begin", self.on_std)
        zoom_listener.connect("update", self.on_std)


    def create_render_buffer(self, image_width:int, image_height:int):
        buff = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, True, 8, image_width, image_height)
        print(buff)
        self.render_surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, image_width, image_height)
        self.render_context = cairo.Context(self.render_surface)
        # self.render_context.set_source_rgb(0,255,0)

        self.render_context.paint()
        self.canvas_size = (image_width, image_height)
        self.org_canvas_size = (image_width, image_height)
        self.canvas_pos = (40,40)
        self.zoom = 20

    def on_zoom(self, gesture:Gtk.GestureZoom, scale):
        print("SCALE", scale)

    # def on_drag_end(self, gesture: Gtk.Gesture, relative_x, relative_y):

    def on_drag(self, gesture: Gtk.Gesture, relative_x, relative_y):
        (is_drag, start_x, start_y) = gesture.get_start_point()
        window_x = relative_x + start_x
        window_y = relative_y + start_y
        (x,y) = self.convert_window_to_canvas_coords((window_x, window_y))
        self.render_context.reset_clip()
        self.render_context.identity_matrix()
        # self.render_context.set_antialias(cairo.ANTIALIAS_NONE)
        self.render_context.rectangle(int(x), int(y), 1, 1)
        self.render_context.set_source_rgb(0, 0, 1)
        self.render_context.fill()
        self.render_context.clip()
        self.render_context.paint()

        self.queue_draw()

    def set_zoom(self, zoom):
        self.zoom = zoom
        self.queue_draw()

    def convert_window_to_canvas_coords(self, coords):
        return ((coords[0]  - self.canvas_pos[0]) * 1/self.zoom, (coords[1] - self.canvas_pos[1]) * 1/self.zoom)

    def on_std(self, *args):
        print("STD: ", args)

    def draw(self, da:Gtk.DrawingArea, cr:cairo.Context, width:int, height:int):
        cr.set_source_rgb(255, 255, 255)
        cr.paint()

        # self.render_surface = self.scale_surface(self.render_surface, 150, 300)
        # (scaled_surface, scaled_size) = self.scale_surface(self.render_surface, self.org_canvas_size, self.zoom)



        # cr.set_antialias(cairo.ANTIALIAS_NONE)
        cr.scale(self.zoom, self.zoom)
        cr.set_source_surface(self.render_surface, self.canvas_pos[0] * 1/self.zoom, self.canvas_pos[1] *  1/self.zoom)
        cr.get_source().set_filter(cairo.Filter.NEAREST)        # pattern.set_filter(cairo.Filter.NEAREST)
        cr.paint()


    # def scale_surface(self, image_surface:cairo.ImageSurface, new_width:int, new_height:int):
    #     width_stride = cairo.Format.stride_for_width(cairo.FORMAT_ARGB32, new_width)
        # print(image_surf`ace.get_data()[3])
    #     surf = cairo.ImageSurface.create_for_data(image_surface.get_data(), cairo.FORMAT_ARGB32, new_width, new_height, width_stride)
    #     return surf

    def scale_surface(self, cairo_surface:cairo.Surface, org_size, scale):
        new_width = int(org_size[0] * scale)
        new_height = int(org_size[1] * scale)

        # pattern1.set_filter(cairo.Filter.NEAREST)

        result = cairo.Surface.create_similar(cairo_surface, cairo.Surface.get_content(cairo_surface),  new_width, new_height)
        cr = cairo.Context(result)

        cr.scale(scale, scale)
        cr.set_source_surface(cairo_surface, 0, 0)
        cr.set_operator(cairo.Operator.SOURCE)
        cr.get_source().set_filter(cairo.Filter.NEAREST)        # pattern.set_filter(cairo.Filter.NEAREST)
        cr.paint()
        return result, (new_width, new_height)



